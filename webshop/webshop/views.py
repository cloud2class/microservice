from flask import Blueprint, request, redirect, render_template, url_for
from flask.json import jsonify as jsonify
from flask.views import MethodView
from jinja2 import TemplateNotFound
import logging
import requests

logging.basicConfig(filename='logs/webshop.log',level=logging.DEBUG)

views = Blueprint('pages',__name__, template_folder='templates')

class Index(MethodView):
    def get(self):
        return render_template('index.html')

def submitform():

    # Perform a stock check
    try:
        item = request.form['item']
        # Removed for demo
        orderrequest = requests.get("http://orders:9000/stockcheck/%s" % item, timeout=1)
        if orderrequest.text != "ok":
            return render_template('outofstock.html')

    # Fail gracefully if the service doesnt respond: please try again later
    except:
        return render_template('failure.html')

    # If we have stock, redirect to purchasing
    render_template('placed.html')

# Html Views
views.add_url_rule('/', view_func=Index.as_view('index'))
views.add_url_rule('/submit', 'submitform', submitform, methods=['POST'])

