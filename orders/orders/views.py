from flask import Blueprint, request, redirect, render_template, url_for
from flask.json import jsonify as jsonify
from flask.views import MethodView
from jinja2 import TemplateNotFound
import logging
import requests
import redis

logging.basicConfig(filename='logs/orders.log',level=logging.DEBUG)

views = Blueprint('pages',__name__, template_folder='templates')

r = redis.StrictRedis(host='redis', port=6379, db=0)

class Stockcheck(MethodView):
    def get(self, stockitem):
        try:
            stockcheck = r.get(stockitem)
            print "got %s" % stockcheck
            if int(stockcheck) <= 0: 
                return ("Could not find item",404)
        except:
             return ("Could not find item",404)
	return ("ok: %s" % stockcheck,200)

# Html Views
views.add_url_rule('/stockcheck/<stockitem>', view_func=Stockcheck.as_view('stockcheck'))

