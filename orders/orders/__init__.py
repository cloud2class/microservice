from flask import Flask, send_from_directory
import logging, ssl, os

logging.basicConfig(filename='logs/orders.log',level=logging.DEBUG)

app = Flask(__name__, static_url_path='')

def register_blueprints(app):
    from orders.views import views
    app.register_blueprint(views)

register_blueprints(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port='8000')
