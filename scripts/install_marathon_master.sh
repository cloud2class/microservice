#!/bin/bash

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv E56151BF
DISTRO=$(lsb_release -is | tr '[:upper:]' '[:lower:]')
CODENAME=$(lsb_release -cs)

echo "deb http://repos.mesosphere.com/${DISTRO} ${CODENAME} main" | \
  tee /etc/apt/sources.list.d/mesosphere.list
add-apt-repository ppa:webupd8team/java
apt-get update -y
apt-get install -y oracle-java8-installer oracle-java8-set-default
apt-get -y update
apt-get -y install mesos marathon

sudo service mesos-slave stop
sudo sh -c "echo manual > /etc/init/mesos-slave.override"
