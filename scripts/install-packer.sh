#!/bin/bash

cd ~
wget https://releases.hashicorp.com/packer/0.9.0/packer_0.9.0_linux_amd64.zip
unzip packer_0.9.0_linux_amd64.zip
sudo cp packer /usr/local/bin/
